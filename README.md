Containers cookiecutter template
================================

A [Cookiecutter][] for bootstrapping new [Toolforge Containers][] services.

Usage
-----
```
$ pipx install cookiecutter
$ cookiecutter https://gitlab.wikimedia.org/toolforge-repos/containers-cookiecutter
  [1/3] service (example):
  [2/3] project_name (Toolforge example container):
  [3/3] year (2024):
$ cd containers-example/
$ git init
$ git remote add origin https://gitlab.wikimedia.org/toolforge-repos/containers-example
$ git add .
$ git commit -m "Bootstrapped project with containers-cookiecutter"
$ git push
```

License
-------
Licensed under the [GPL-3.0-or-later][] license. See [COPYING][] for the full
license.

[Cookiecutter]: https://github.com/cookiecutter/cookiecutter
[Toolforge Containers]: https://wikitech.wikimedia.org/wiki/Tool:Containers
[GPL-3.0-or-later]: https://www.gnu.org/licenses/gpl-3.0.html
[COPYING]: COPYING
